VAR delpok = "unbekannt"
VAR impacted = "unbekannt"
VAR dectool = "unbekannt"


Decision making in large business groups, an interactive adventure

Once upon a time, you had something to decide. As you think about it, how many people will be impacted by it?

* Just yourself.
~ impacted = "me"
-> metool
* The team you are part of.
~ impacted = "team"
-> delpoker
* More people.
    ~ impacted =  "more"
    -> delpoker

== metool

Just decide it for yourself. Too difficult? Throw a coin.
-> END

== mepoker 

That makes it only 3 different variants to think about.

* Just TELL them my decision you did alone. They have to live with that.
-> END

* SELL them your idea, because you find it important that they know why you have decided that way.
-> END

* Or CONSULT them first and decide alone afterwards with their input in mind.
-> END

== delpoker ==

Thinking about the power you have over those impacted people. You remember the different cards of delegation poker. Do  you want to make this decision

* Alone
-> mepoker

* Together with those impacted people to have a clear decision, noone is against. (knowing this kind of harmony takes its time)
-> toolquest

* Delegate that completely to a group of people not including yourself.
-> toolquest

== toolquest

Thinking about the decision to be made. Is it a

* Yes or NO Decision
-> Consent

* A decision with more than two options to choose from?
-> ohm

== Consent
Helping to find a decision everybody can AGREE upon you can use the sociocratic consent method. described here: "patterns.sociocracy30.org/consent-decision-making.html" 

-> END

==  ohm
You can turn it around and ask every participant to think about how much they oppose those options. Everyone distributes parts of 100% resistance to each of the options. The option with minimal resistance has won. (pro tip: the winning option can be turned into an yes/no proposal and put into consent decision making, so the best option is choosen in absence of any objections.)


-> END
